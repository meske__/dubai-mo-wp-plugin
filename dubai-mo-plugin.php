<?php
  /*
  Plugin Name:       C RSS Feed
  Description:       Plugin for categorizing rss feeds with cust UI.
  Version:           1.0.0
  Requires at least: 5.2
  Author:            Mesic Selim
  License:           GPL v2 or later
  License URI:       https://www.gnu.org/licenses/gpl-2.0.html
  Text Domain:       C RSS Feed
  */

  defined ( 'ABSPATH' ) or die('Hey, what are you doing here?');
  define('SM24_CRSS_PLUGIN_PATH', plugin_dir_path(__FILE__));
  define('SM24_CRSS_PLUGIN_URL', plugin_dir_url(__FILE__));
  define('SM24_CRSS_PLUGIN_NAME', plugin_basename(__FILE__));


  function dubai_mo_frontend_section(){

    $all_fields = get_option('dubai_mo_cards_list');
    $all_desc_fields = get_option('dubai_mo_cards_desc_list');

    wp_enqueue_style( 'dubai_mo_admin' );
    wp_enqueue_script('dubai-mo-front-script');
    $content_dynamic = '';
    $counter = 0;
    foreach ($all_fields as $field) {
      $content_dynamic .= "<div class='dubai-mo-img-wrap'>
        <div class='dubai-mo-section'>
          <img src='".get_option($field['id'])."'/>
          <p>".$field['title']."</p>
        </div>
        <div class='dubai-mo-section'>
          <p>".$all_desc_fields[$counter][1] ."</p>
        </div>
      </div>";
      $counter += 1;
    };

    $content = "<div class='dubai-mo-wrap'>"
    . $content_dynamic ."</div>";

    return $content;
  }


  function dubai_mo_categories_section(){

    wp_enqueue_style( 'dubai_mo_admin' );
    wp_enqueue_script('dubai-mo-front-script');

    $content = "<div class='dubai-mo-wrap'>
      <div class='dubai-mo-img-wrap'>
        <div class='dubai-mo-section'>
          <img src='".$articles_image_url."'/>
          <p>Arti241412cles</p>
        </div>
        <div class='dubai-mo-section'>
          <p>".$articles_description."</p>
        </div>
      </div>

      <div class='dubai-mo-img-wrap'>
        <div class='dubai-mo-section'>
          <img src='".$podcasts_image_url."'/>
          <p>Artic214214les</p>
        </div>
        <div class='dubai-mo-section'>
          <p>".$podcasts_description."</p>
        </div>
      </div>

      <div class='dubai-mo-img-wrap'>
        <div class='dubai-mo-section'>
          <img src='".$tweets_image_url."'/>
          <p>Arti241241cles</p>
        </div>
        <div class='dubai-mo-section'>
          <p>".$tweets_description."</p>
        </div>
      </div>

      <div class='dubai-mo-img-wrap'>
        <div class='dubai-mo-section'>
          <img src='".$videos_image_url."'/>
          <p>221421</p>
        </div>
        <div class='dubai-mo-section'>
          <p>".$videos_description."</p>
        </div>
      </div>
    </div>";

    return $content;
  }
  function dubai_mo_call_categories(){
    add_shortcode('dubai_mo_categories', 'dubai_mo_categories_section');
  }

  add_action('init', 'dubai_mo_call_categories');

  function add_new_field(){
    $new_fields = $_POST['data'];

    $list = get_option('dubai_mo_cards_list');
    $desc_list = get_option('dubai_mo_cards_desc_list');

    foreach ($new_fields as $field) {
      $new_field = array('id' => $field[2], 'title' => $field[0], 'callback' => 'input_field_image', 'slug' => 'dubai_mo_unique',
      'section_id' => 'dubai_mo_card_images_section', 'array' => array(
          'label_for' => $field[2],
          'class' => 'dubai-field'));
      $new_field_desc = [$field[2], 'Sample description...'];
      array_push($list, $new_field);
      array_push($desc_list, $new_field_desc);
    }
    echo get_option('dubai_mo_cards_list');

    update_option('dubai_mo_cards_list', $list);
    update_option('dubai_mo_cards_desc_list', $desc_list);

    wp_die();
  }
  add_action('wp_ajax_add_fields', 'add_new_field');
  add_action('wp_ajax_nopriv_add_fields', 'add_new_field');

  function delete_field() {
    global $wpdb;
    $delete_field = $_POST['data'];

    $list = get_option('dubai_mo_cards_list');
    $desc_list = get_option('dubai_mo_cards_desc_list');

    $new_list = array();
    $new_desc_list = array();

    $counter = 0;
    foreach($list as $item) {
      if($item['id'] == $delete_field) {
        $wpdb->query("DELETE FROM $wpdb->options WHERE option_name = '$delete_field'");
        $counter = $counter + 1;
        continue;
      }
      array_push($new_desc_list, $desc_list[$counter]);
      array_push($new_list, $item);
      $counter = $counter + 1;
    }

    update_option('dubai_mo_cards_list', $new_list);
    update_option('dubai_mo_cards_desc_list', $new_desc_list);

    wp_die();
  }
  add_action('wp_ajax_delete_fields', 'delete_field');
  add_action('wp_ajax_nopriv_delete_fields', 'delete_field');

  function ajax_call_func(){
     echo do_shortcode('[dubai_mo_categories]');
     die();
  }
  add_action('wp_ajax_dubai', 'ajax_call_func');
  add_action('wp_ajax_nopriv_dubai', 'ajax_call_func');

  function dubai_mo_call_shortcode(){
    add_shortcode('dubai_mo_section', 'dubai_mo_frontend_section');
  }

  add_action('init', 'dubai_mo_call_shortcode');

  function get_cards(){

    if(get_option('dubai_mo_cards_list') == false){
      add_option('dubai_mo_cards_list', []);
    }

    if(get_option('dubai_mo_cards_desc_list') == false) {
      add_option('dubai_mo_cards_desc_list', []);
    }

    $all_cards = get_option('dubai_mo_cards_list');

    return $all_cards;
  }

  function register_custom_fields(){

    if( get_option('dubai_mo_settings') == false) {
      add_option('dubai_mo_settings');
    }

    add_settings_section('dubai_mo_card_images_section', 'Cards', 'admin_section_callback', 'dubai_mo_unique');

    /*


    //Custom field for image url...
    add_settings_field('dubai_mo_articles', 'Articles card image', 'input_field_image', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_articles',
        'class' => 'my-cls'));
    //Custom fields for card description...
    add_settings_field('dubai_mo_articles_desc', 'Articles description', 'input_field_desc', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_articles_desc',
        'class' => 'dubai-mo-desc'));

    //Custom field for image url...
    add_settings_field('dubai_mo_podcasts', 'Podcast card image', 'input_field_image', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_podcasts',
        'class' => 'my-cls'));
    //Custom fields for card description...
    add_settings_field('dubai_mo_podcasts_desc', 'Podcast description', 'input_field_desc', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_podcasts_desc',
        'class' => 'dubai-mo-desc'));

    //Custom field for image url...
    add_settings_field('dubai_mo_tweets', 'Tweets card image', 'input_field_image', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_tweets',
        'class' => 'my-cls'));
    //Custom fields for card description...
    add_settings_field('dubai_mo_tweets_desc', 'Tweets description', 'input_field_desc', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_tweets_desc',
        'class' => 'dubai-mo-desc'));

    //Custom field for image url...
    add_settings_field('dubai_mo_videos', 'Videos card image', 'input_field_image', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_videos',
        'class' => 'my-cls'));
    //Custom fields for card description...
    add_settings_field('dubai_mo_videos_desc', 'Videos description', 'input_field_desc', 'dubai_mo_unique', 'dubai_mo_card_images_section', array(
        'label_for' => 'dubai_mo_videos_desc',
        'class' => 'dubai-mo-desc'));

    register_setting( 'dubai_mo_settings', 'dubai_mo_articles');
    register_setting( 'dubai_mo_settings', 'dubai_mo_podcasts');
    register_setting( 'dubai_mo_settings', 'dubai_mo_tweets');
    register_setting( 'dubai_mo_settings', 'dubai_mo_videos');

    register_setting( 'dubai_mo_settings', 'dubai_mo_articles_desc');
    register_setting( 'dubai_mo_settings', 'dubai_mo_podcasts_desc');
    register_setting( 'dubai_mo_settings', 'dubai_mo_tweets_desc');
    register_setting( 'dubai_mo_settings', 'dubai_mo_videos_desc');
    */
    if(get_cards() == false){
      return;
    }else {
      foreach(get_cards() as $card) {
        add_settings_field($card['id'], $card['title'], $card['callback'], $card['slug'], $card['section_id'], $card['array']);
        register_setting('dubai_mo_settings', $card['id']);
      }
    }

  }
  //Textarea field callback for card description...
  function input_field_desc($args){
    $options = get_option($args['label_for']);
    echo '<textarea type="text" name="'.$args['label_for'].'" placeholder="Input the card description">'.$options.'</textarea>';
  }
  //Input field callback for image url...
  function input_field_image($args){
    $options = get_option($args['label_for']);
    echo '<input type="text" name="'.$args['label_for'].'" value="'.$options.'" placeholder="Input the image url.">';
  }

  function add_settings_link($links) {
    $setting_link = '<a href="admin.php?page=dubai_mo_unique">Settings</a>';
    array_push($links, $setting_link);
    return $links;
  }

  $filter_name = 'plugin_action_links_' . SM24_CRSS_PLUGIN_NAME;
  add_filter($filter_name, 'add_settings_link');

  function add_option_group($input){
      return $input;
  }

  function admin_section_callback(){
      echo '';
  }

  function add_admin_page_dubai_mo(){
    add_menu_page('dubai_mo_plugin', 'Dubai Mo', 'manage_options', 'dubai_mo_unique', 'admin_template', 'dashicons-rss', 100);
  }

  function admin_template(){
    require_once SM24_CRSS_PLUGIN_PATH . 'templates/admin_menu_template.php';
  }

  function activate_plugin_dubai_mo(){
    flush_rewrite_rules();
  }
    function deactivate_plugin_dubai_mo(){
    flush_rewrite_rules();
  }

  include(SM24_CRSS_PLUGIN_PATH . 'inc/enqueue-style.php');
  include(SM24_CRSS_PLUGIN_PATH . 'inc/enqueue-scripts.php');

  add_action('admin_init', 'register_custom_fields');
  add_action('admin_menu', 'add_admin_page_dubai_mo');

  register_activation_hook(__FILE__, 'activate_plugin_dubai_mo');
  register_deactivation_hook(__FILE__, 'deactivate_plugin_dubai_mo');


?>
