document.addEventListener("DOMContentLoaded", ready);

function ready() {
  console.log('Admin Script is loaded...');

  let add_button = document.querySelector('.add-field-dubai');
  let fake_submit_btn = document.querySelector('.fake-submit-dubai');
  let real_submit = document.querySelector('.hide-me-dubai');

  //Add info tab to tbody...
  let tbody = document.querySelector('tbody');
  let info_tab = document.createElement('div');
  let info_tab_inner = document.createElement('div');
  info_tab.classList.add('additional-info');
  info_tab.append();
  info_tab.append(info_tab_inner);
  info_tab_inner.classList.add('additional-info-inner');
  if(tbody){
    tbody.append(info_tab);
  }

  fake_submit_btn.addEventListener('click', () => {
    let new_input_fields = document.querySelectorAll('.new-input-field');
    let single_field_holder = document.querySelectorAll('.field-holder');

    (new_input_fields.length > 0) ? save_new_fields() : classic_submit();

    function classic_submit(){
      real_submit.click();
    }

    function save_new_fields(){
      let all_data = [];

      for(let i = 0; i < single_field_holder.length; i++) {
        let title = single_field_holder[i].querySelector('.title-dubai-input');
        let value = single_field_holder[i].querySelector('.value-dubai-input');
        let single_field = [title.value, value.value, title.value.toLowerCase().split(' ').join('_')];

        all_data.push(single_field);
      }

      (function ($) {
        $.ajax({
          url: ajaxurl,
          data: {
            action: 'add_fields',
            data: all_data
          },
          method: 'POST',
          success: function(response) {
            real_submit.click();
          },
          error: function(err) {console.log(err)}
        })
      })(jQuery)
    };

  });

  //Add new field ...
  add_button.addEventListener('click', () => {
    let wrap = document.querySelector('.wrap > form');

    let new_field = document.createElement('input');
    let new_field_2 = document.createElement('input');
    let field_wrap = document.createElement('div');

    new_field.classList.add('new-input-field', 'title-dubai-input');
    new_field_2.classList.add('new-input-field', 'value-dubai-input');
    field_wrap.classList.add('flex');
    field_wrap.classList.add('field-holder');

    new_field.setAttribute('placeholder', 'Add the card title here.')
    new_field_2.setAttribute('placeholder', 'Add the card image url here.')
    field_wrap.append(new_field);
    field_wrap.append(new_field_2);
    appendBefore(field_wrap, document.querySelector('p.add-field-dubai-submit'));

    let all_input_titles = document.querySelectorAll('.title-dubai-input');
    let all_input_values = document.querySelectorAll('.value-dubai-input');

    for(let i = 0; i < all_input_titles.length; i++) {
      all_input_titles[i].addEventListener('input', (e) => {
        let second_input = e.target.parentElement.childNodes[1];
        let name = e.target.value.toLowerCase().split(' ').join('_');
        second_input.setAttribute('name', name);
      });
    }
    for(let i = 0; i < all_input_values.length; i++) {
      all_input_values[i].addEventListener('input', (e) => {
        all_input_values[i].setAttribute('value', e.target.value);
      });
    }
  });

  //Adding table items...
  let custom_fields = document.querySelectorAll('.dubai-field');
  for(let f = 0; f < custom_fields.length; f++) {

    let delete_img = document.createElement('td');
    let img_preview_wrap = document.createElement('td');
    let info = document.createElement('td');

    let img = document.createElement('img');
    let preview_img = document.createElement('img');
    let info_img = document.createElement('img');


    let valueOfCurrentInput = custom_fields[f].childNodes[1].querySelector('input').value;
    if(valueOfCurrentInput) {
      preview_img.src = valueOfCurrentInput;
    }else {
      preview_img.src = jsforwp_globals.plugin_path + 'icons/placeholder.jpg';
    }

    img_preview_wrap.classList.add('img-placeholder');
    delete_img.classList.add('delete-dubai-field');
    info.classList.add('card-info');

    img_preview_wrap.append(preview_img);
    info.append(info_img);
    delete_img.append(img);

    img.src = jsforwp_globals.plugin_path + 'icons/delete.png';
    info_img.src = jsforwp_globals.plugin_path + 'icons/information.png';

    custom_fields[f].append(img_preview_wrap);
    custom_fields[f].append(info);
    custom_fields[f].append(delete_img);
  }


  //Create additional info element...
  let additional = document.querySelector('.additional-info');
  //Create elements for additional info...
  let submit = document.createElement('p');
  let input_submit = document.createElement('input');

  //Add all classes, attributes and similar...
  submit.classList.add('submit', 'inner-info');
  input_submit.classList.add('button', 'button-primary', 'info-save-button');
  input_submit.setAttribute('type', 'button');
  input_submit.setAttribute('value', 'Save');

  //Append elements...
  submit.append(input_submit);
  if(info_tab_inner){
    info_tab_inner.append(submit);
  }


  //Handle open/close info tab...
  let allInfo = document.querySelectorAll('.card-info');
  for(let t = 0; t < allInfo.length; t++) {
    allInfo[t].addEventListener('click', (e) => {

      additional.style.height = '100%';

      let save_button = document.querySelector('.info-save-button');
      save_button.addEventListener('click', () => {
        additional.style.height = '0%';
      });


    });
  }
  //Handle field delete ...
  let allTrash = document.querySelectorAll('.delete-dubai-field');
  for(let r = 0; r < allTrash.length; r++) {
    allTrash[r].addEventListener('click', () => {
      let td = allTrash[r].parentElement.childNodes[1];
      let inputFieldName = td.querySelector('input').getAttribute('name');
      let data = inputFieldName;
      allTrash[r].parentNode.style.background = 'rgba(255, 0, 0, 0.54)';
      (function ($) {
        $.ajax({
          url: ajaxurl,
          data: {
            action: 'delete_fields',
            data: data
          },
          method: 'POST',
          success: function(response) {
            allTrash[r].parentNode.remove();
          },
          error: function(err) {console.log(err)}
        })
      })(jQuery)
    });
  }

  //Change image preview on src input...
  let all_url_input_fields = document.querySelectorAll('.dubai-field input');
  for(let p = 0; p < all_url_input_fields.length; p++) {
    all_url_input_fields[p].addEventListener('input', (e) => {
      let current_img_preview = e.target.parentNode.parentNode.querySelector('.img-placeholder > img');
      current_img_preview.src = e.target.value;
    });
  }
}


function appendBefore(newElement, targetElement){
  targetElement.parentNode.insertBefore(newElement, targetElement);
}
