<?php

  function dubai_mo_admin_scripts(){
    wp_enqueue_script('dubai-mo-admin-script', SM24_CRSS_PLUGIN_URL . 'admin/js/dubai_mo_admin_script.js', [], time());
    wp_localize_script( 'dubai-mo-admin-script', 'jsforwp_globals', array( 'plugin_path' => plugin_dir_url(__DIR__) ) );
  }
  add_action('admin_enqueue_scripts', 'dubai_mo_admin_scripts');

  function dubai_mo_frontend_scripts(){
    wp_register_script('dubai-mo-front-script', SM24_CRSS_PLUGIN_URL . 'frontend/js/dubai_mo_frontend_script.js', array('jquery'), time());
  }
  function localize_script(){
    //wp_localize_script( 'dubai-mo-front-script', 'jsforwp_globals', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

  }
  add_action('wp_enqueue_scripts', 'localize_script');
  add_action('init', 'dubai_mo_frontend_scripts');
?>
