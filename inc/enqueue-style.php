<?php

  function dubai_mo_admin_styles(){
    wp_enqueue_style('dubai_mo_admin_style', SM24_CRSS_PLUGIN_URL . 'admin/css/dubai_mo_admin_style.css', [], time());
  }
  add_action('admin_enqueue_scripts', 'dubai_mo_admin_styles', 100);

  function dubai_mo_frontend_styles(){
    wp_enqueue_style('dubai_mo_frontend', SM24_CRSS_PLUGIN_URL . 'frontend/css/dubai_mo_frontend_style.css', [], time());
  }
  add_action('wp_enqueue_scripts', 'dubai_mo_frontend_styles');

?>
