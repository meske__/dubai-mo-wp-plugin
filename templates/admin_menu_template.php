<div class="wrap">
    <h1>Hello World.</h1>

    <form method="post" action="options.php">
    <?php
        settings_errors();
        settings_fields('dubai_mo_settings');
        do_settings_sections('dubai_mo_unique');
        ?> <p class="submit add-field-dubai-submit"><input type="button" class="button button-primary add-field-dubai" value="Add New Field"></p> <?php
        ?> <p class="submit"><input type="button" class="button button-primary fake-submit-dubai" value="Save Changes"></p> <?php
        submit_button('Save Changes', 'button-primary hide-me-dubai');
    ?>
    </form>

</div>
