console.log('Frontend Script is loaded...');
let cards = document.querySelectorAll('.dubai-mo-img-wrap');
for(let i = 0; i<cards.length; i++){
  cards[i].addEventListener('click', () => {

    (function ($) {
      $.ajax({
        url: jsforwp_globals.ajax_url,
        data: {
          action: 'dubai'
        },
        method: 'POST',
        success: function(response) {
          let wrapper = document.querySelector('.dubai-mo-wrap');
          wrapper.innerHTML = response;
        },
        error: function(err) {console.log(err)}
      })
    })(jQuery)
  });
}
